package com.example.springboot.dao;

import java.util.List;
import java.util.Map;


public interface EmployeeDao {
    public List<Map<String, Object>> getAllEmp();
}
