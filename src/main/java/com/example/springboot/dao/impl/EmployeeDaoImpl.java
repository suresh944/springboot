package com.example.springboot.dao.impl;

import com.example.springboot.dao.EmployeeDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@Repository
public class EmployeeDaoImpl implements EmployeeDao {
    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public List<Map<String, Object>> getAllEmp()  {
        return jdbcTemplate.queryForList("select * from HumanResources.Employee");

//        String sql = "select * from HumanResources.Employee";
//        System.out.println("\n\n\n\t\tGoooooooooooD =========> " + jdbcTemplate.queryForList(sql));
//            return null;
    }
}
