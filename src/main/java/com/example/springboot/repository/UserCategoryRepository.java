package com.example.springboot.repository;

import com.example.springboot.entity.UserCategoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;



public interface UserCategoryRepository extends JpaRepository<UserCategoryEntity , Integer> {



}
