package com.example.springboot.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class EmployeeDTO {
    @JsonProperty("employee_id")
    private Integer id;
    @JsonProperty("first_name")
    private String firstName;
}
