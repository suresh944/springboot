package com.example.springboot.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class Old_UserDTO implements Serializable {
    @JsonProperty("id")
    int id;

    @JsonProperty("name")
    String name;

    @JsonProperty("user_name")
    String userName;

    public Old_UserDTO() {
    }
    public Old_UserDTO(int id, String name){
        this.id=id;
        this.name=name;
    }
    public Old_UserDTO(int id, String name, String userName){
        this.id=id;
        this.name=name;
        this.userName=userName;
    }
}
