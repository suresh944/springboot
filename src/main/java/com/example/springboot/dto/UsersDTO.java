package com.example.springboot.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UsersDTO {
    @JsonProperty("user_code")
    private Integer userCode;

    @JsonProperty("user_loginname")
    private String userLoginName;

    @JsonProperty("user_category_id")
    private Integer userCategoryId;


    @JsonProperty("employee_id")
    private Integer employeeId;

}
