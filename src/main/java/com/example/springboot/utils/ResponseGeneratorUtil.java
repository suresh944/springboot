package com.example.springboot.utils;

import com.example.springboot.dto.ResponseDTO;
import com.example.springboot.dto.Old_UserDTO;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ResponseGeneratorUtil {
    public ResponseDTO generateResponse(boolean errorStatus, String code, String message, List<Object> objectList) {
        ResponseDTO responseDTO = new ResponseDTO();

        responseDTO.setErrorStatus(errorStatus);
        responseDTO.setCode(code);
        responseDTO.setMessage(message);
        responseDTO.setData(objectList);

        return responseDTO;
    }
}
