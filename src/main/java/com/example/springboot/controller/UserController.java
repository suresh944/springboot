package com.example.springboot.controller;

import com.example.springboot.dto.ResponseDTO;
import com.example.springboot.dto.UsersDTO;
import com.example.springboot.entity.UserCategoryEntity;
import com.example.springboot.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@ResponseBody
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping(value = "/categoryUser")
    public List<UserCategoryEntity> getUserCategoryWise() {
        return userService.getUserByCategory();
    }

    @PostMapping(value = "/add")
    public ResponseDTO addUsers(@RequestBody UsersDTO usersDTO) {
        return userService.addUser(usersDTO);
    }


}
