package com.example.springboot.controller;

import com.example.springboot.dto.Old_UserDTO;
import com.example.springboot.dto.ResponseDTO;
import com.example.springboot.service.Old_UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * The type User controller.
 */
@ResponseBody
@Controller
@RequestMapping("/userStatic")
public class Old_UserController {
    /**
     * The User service.
     */
    @Autowired
    Old_UserService oldUserService;

    /**
     * Get all user response dto.
     *
     * @return the response dto
     */
    @GetMapping(value = "/getAllUser")
    public ResponseDTO getAllUser() {
        return oldUserService.findUser();
    }

    /**
     * Find user by id response dto.
     *
     * @param id the id
     * @return the response dto
     */
    @GetMapping(value = "/getUserById/{id}")
    public ResponseDTO findUserById(@PathVariable int id) {
        return oldUserService.findUseById(id);
    }

    /**
     * Insert response dto.
     *
     * @param userDTO the user dto
     * @return the response dto
     */
    @PostMapping("/add")
    public ResponseDTO insert(@RequestBody Old_UserDTO userDTO) {
        return oldUserService.insert(userDTO);
    }

    /**
     * Update response dto.
     *
     * @param userDTO the user dto
     * @return the response dto
     */
    @PutMapping("/edit")
    public ResponseDTO update(@RequestBody Old_UserDTO userDTO) {
        return oldUserService.insert(userDTO);
    }

    /**
     * Insert response dto.
     *
     * @param userDTOList the user dto list
     * @return the response dto
     */
    @PostMapping("/addAll")
    public ResponseDTO insert(@RequestBody List<Old_UserDTO> userDTOList) {
        return oldUserService.insert(userDTOList);
    }


    /*
    //Pending to Handle non id user
    @RequestMapping("/delete/{id}")
    public void remove(@PathVariable int id){
        oldUserService.delete(id);
    }
    */
}
