package com.example.springboot.controller;

import com.example.springboot.dto.EmployeeDTO;
import com.example.springboot.dto.ResponseDTO;
import com.example.springboot.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/emp")
public class EmployeeController {

    @Autowired
    EmployeeService employeeService;

    @GetMapping(value = "/all")
    @Procedure(value = "MediaType.APPLICATION_JSON")
    public List<Map<String, Object>> getEmp() {
        return employeeService.getEmployee();
    }

    @GetMapping(value = "/allByJPA")
    public List<EmployeeDTO> getAllByJPA() {
        return employeeService.getEmployeeByJPA();
    }

    @PostMapping(value = "/add")
    public EmployeeDTO addEmployee(@RequestBody EmployeeDTO employeeDTO){
        return employeeService.addEmployee(employeeDTO);
    }

    @DeleteMapping(value = "/remove")
    public ResponseDTO removeEmployee(@RequestBody EmployeeDTO employeeDTO){
        return employeeService.removeEmployee(employeeDTO.getId());
    }

    @PutMapping(value = "/edit")
    public ResponseDTO editEmployee(@RequestBody EmployeeDTO employeeDTO){
        return employeeService.editEmployee(employeeDTO.getId());
    }
}
