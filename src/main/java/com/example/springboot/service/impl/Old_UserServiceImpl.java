package com.example.springboot.service.impl;

import com.example.springboot.dto.Old_UserDTO;
import com.example.springboot.dto.ResponseDTO;
import com.example.springboot.exception.UserNotFoundException;
import com.example.springboot.service.Old_UserService;
import com.example.springboot.utils.ResponseGeneratorUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class Old_UserServiceImpl implements Old_UserService {

    @Autowired
    ResponseGeneratorUtil responseGeneratorUtil;
    List<Old_UserDTO> userList = new ArrayList<Old_UserDTO>();

    public Old_UserServiceImpl() {
        Old_UserDTO user = new Old_UserDTO(101, "Alpesh", "Ahmedabad");
        userList.add(user);
        userList.add(new Old_UserDTO(102, "Bipin", "Bharuch"));
        userList.add(new Old_UserDTO(103, "Chirag", "Chennai"));
        userList.add(new Old_UserDTO(104, "Dinesh", "Delhi"));
        userList.add(new Old_UserDTO(105, "Elesh", "Surat"));
        //        list.addAll( list.add(new Old_UserDTO(222,"Bipin")));
    }

    @Override
    public ResponseDTO findUser() {
        return responseGeneratorUtil.generateResponse(false, HttpStatus.OK.toString(), "return all User successfully", Collections.singletonList(userList));
    }

    @Override
    public ResponseDTO findUseById(Integer id) {

        List<Old_UserDTO> userDTOSList = new ArrayList<Old_UserDTO>();
        Old_UserDTO findUserDTO = userList.stream().filter(userDTO -> userDTO.getId() == id).findFirst().orElse(null);

        try {
            if (findUserDTO.getId() == id) {
                userDTOSList.add(findUserDTO);
                return responseGeneratorUtil.generateResponse(false, HttpStatus.ACCEPTED.toString(), "User Found with Following Detail", Collections.singletonList(userDTOSList));
            }
        } catch (Exception ex) {
            throw new UserNotFoundException(id + " user not Available");
        }
        return null;
    }

    @Override
    public ResponseDTO insert(Old_UserDTO userDTO) {
        //find Old_UserDTO is Available or not

        boolean flag = true;
        for (Old_UserDTO getUser : userList)
            if (getUser.getId() == userDTO.getId())
                flag = false;

        if (flag) {
            //insert user
            Old_UserDTO user = new Old_UserDTO(userDTO.getId(), userDTO.getName(), userDTO.getUserName());
            userList.add(user);
        } else {
            //Update Old_UserDTO
            update(userDTO);
        }

        return responseGeneratorUtil.generateResponse(false, HttpStatus.CREATED.toString(), "User Add Successfully", null);
    }

    @Override
    public ResponseDTO update(Old_UserDTO userDTO) {
        userList.stream().forEach(user1 -> {
            if (user1.getId() == userDTO.getId()) {
                user1.setUserName(userDTO.getUserName());
                user1.setName(userDTO.getName());
            }
        });
        return null;
    }

    public ResponseDTO insert(List<Old_UserDTO> userDTOList) {
        //find Old_UserDTO is Available or not
        for (Old_UserDTO getUser : userDTOList)
            insert(getUser);
        return responseGeneratorUtil.generateResponse(false, HttpStatus.CREATED.toString(), userDTOList.size() + " User Add Successfully", null);
    }

    @Override
    public void delete(int id) {
        //remove by id
        userList.removeIf(user -> user.getId() == id);
    }

}
