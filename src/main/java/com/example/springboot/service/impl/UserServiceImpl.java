package com.example.springboot.service.impl;

import com.example.springboot.dto.ResponseDTO;
import com.example.springboot.dto.UsersDTO;
import com.example.springboot.entity.UserCategoryEntity;
import com.example.springboot.entity.UsersEntity;
import com.example.springboot.exception.UserNotFoundException;
import com.example.springboot.repository.UserCategoryRepository;
import com.example.springboot.repository.UsersRepository;
import com.example.springboot.service.UserService;
import com.example.springboot.utils.ResponseGeneratorUtil;
import org.hibernate.exception.ConstraintViolationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import java.util.List;


@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserCategoryRepository userCategoryRepository;

    @Autowired
    UsersRepository usersRepository;

    @Autowired
    ResponseGeneratorUtil responseGeneratorUtil;

    private final ModelMapper modelMapper = new ModelMapper();

    @Override
    public List<UserCategoryEntity> getUserByCategory(){
        return userCategoryRepository.findAll();
    }

    @Override
    public ResponseDTO addUser(UsersDTO usersDTO) throws ConstraintViolationException {
        UsersEntity usersEntity = UserEntityFromDTO(usersDTO);
        String message = "";
        try {
            usersRepository.save(usersEntity);
            message = "Saved Success";
//        }catch (org.hibernate.exception.ConstraintViolationException ex){
        }catch (org.springframework.dao.DataIntegrityViolationException ex){
            message= "Duplicate User Found :::::::;:: " + ex.getLocalizedMessage();

            System.out.println("\n\n Error AT Save User :: Duplicate User Found"+ ex.toString());


        }catch (Exception ex){
            message= ex.getLocalizedMessage();
            System.out.println("\n\n Error AT Save User :: " + ex.toString());

        }finally {
            return responseGeneratorUtil.generateResponse(false, HttpStatus.OK.toString(),message,null);
        }

    }

    private UsersEntity UserEntityFromDTO(UsersDTO usersDTO) {
        return modelMapper.map(usersDTO,UsersEntity.class);
    }

}
