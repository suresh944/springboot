package com.example.springboot.service.impl;

import com.example.springboot.dao.EmployeeDao;
import com.example.springboot.dto.EmployeeDTO;
import com.example.springboot.dto.ResponseDTO;
import com.example.springboot.entity.EmployeeEntity;
import com.example.springboot.repository.EmployeeRepository;
import com.example.springboot.service.EmployeeService;
import com.example.springboot.utils.ResponseGeneratorUtil;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * The type Employee service.
 */

@Service
public class EmployeeServiceImpl implements EmployeeService {

    /**
     * The Employee dao.
     */
    @Autowired
    EmployeeDao employeeDao;

    /**
     * The Employee repository.
     */
    @Autowired
    EmployeeRepository employeeRepository;
    @Autowired
    ResponseGeneratorUtil responseGeneratorUtil;
    private final ModelMapper modelMapper = new ModelMapper();

    Boolean errorStatus=false;
    String httpStatusCode = HttpStatus.OK.toString();
    String message = "Data saved Successfully";
    Object data=null;

    @Override
    public List<Map<String, Object>> getEmployee() {
        System.out.println("\n\n\n");
        return employeeDao.getAllEmp();
    }

    @Override
    public List<EmployeeDTO> getEmployeeByJPA() {
        return employeeRepository.findAll()
                .stream()
                .map(this::getEntityToDTO)
                .collect(Collectors.toList());
    }

    @Override
    public EmployeeDTO addEmployee(EmployeeDTO employeeDTO) {
        EmployeeEntity employeeEntity = getDtoFromEntity(employeeDTO);
        EmployeeEntity result = employeeRepository.save(employeeEntity);
        return getEntityToDTO(result);
    }

    @Override
    public ResponseDTO removeEmployee(Integer employeeId) {
        EmployeeEntity employeeEntity = new EmployeeEntity();
        employeeEntity.setId(employeeId);
        try {
            employeeRepository.delete(employeeEntity);
            message="employee Detail Remove Successful";
        } catch (Exception exception) {
            System.out.println("\nException :: " + exception.toString());
        } finally {
            return responseGeneratorUtil.generateResponse(errorStatus,httpStatusCode,message, Collections.singletonList(data));
        }
    }

    @Override
    public ResponseDTO editEmployee(Integer employeeId) {
        EmployeeEntity employeeEntity = new EmployeeEntity();
        employeeEntity.setId(employeeId);
        employeeEntity.setDepartmentMapping_id(67977);
        try {
            employeeRepository.save(employeeEntity);
            message = message.concat(" for :: ".concat(employeeId.toString()));
        }catch (Exception exception){
            message = "Employee Exception :: ";
            httpStatusCode= HttpStatus.UNAVAILABLE_FOR_LEGAL_REASONS.toString();
        }finally {
            return responseGeneratorUtil.generateResponse(errorStatus,httpStatusCode,message, null);
        }

    }

    /*PostDto postDto = modelMapper.map(post, PostDto.class);
        postDto.setSubmissionDate(post.getSubmissionDate(),
            userService.getCurrentUser().getPreference().getTimezone());*/

    private EmployeeDTO getEntityToDTO(EmployeeEntity employeeEntity) {
        EmployeeDTO employeeDTO = new EmployeeDTO();
        employeeDTO = modelMapper.map(employeeEntity, EmployeeDTO.class);
        return employeeDTO;
    }

    private EmployeeEntity getDtoFromEntity(EmployeeDTO employeeDTO) {
        return modelMapper.map(employeeDTO, EmployeeEntity.class);
    }

}
