package com.example.springboot.service;

import com.example.springboot.dto.EmployeeDTO;
import com.example.springboot.dto.ResponseDTO;
import com.example.springboot.entity.EmployeeEntity;

import java.util.List;
import java.util.Map;

public interface EmployeeService {
    public List<Map<String, Object>> getEmployee();
    //For JPA
//    public List<EmployeeEntity> getEmployeeByJPA();
    public List<EmployeeDTO> getEmployeeByJPA();

    EmployeeDTO addEmployee(EmployeeDTO employeeDTO);

    ResponseDTO removeEmployee(Integer employeeId);

    ResponseDTO editEmployee(Integer employeeId);
}
