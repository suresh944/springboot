package com.example.springboot.service;

import com.example.springboot.dto.Old_UserDTO;
import com.example.springboot.dto.ResponseDTO;

import java.util.List;


public interface Old_UserService {
    public ResponseDTO findUser();
    public ResponseDTO findUseById(Integer id);
    ResponseDTO insert(Old_UserDTO userDTO);
    ResponseDTO insert(List<Old_UserDTO> userDTOList);
    ResponseDTO update(Old_UserDTO userDTO);

    void delete(int id);
}
