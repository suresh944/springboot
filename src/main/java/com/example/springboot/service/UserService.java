package com.example.springboot.service;

import com.example.springboot.dto.ResponseDTO;
import com.example.springboot.dto.UsersDTO;
import com.example.springboot.entity.UserCategoryEntity;
import org.hibernate.exception.ConstraintViolationException;

import java.util.List;

public interface UserService {

    List<UserCategoryEntity> getUserByCategory();

    ResponseDTO addUser(UsersDTO usersDTO) throws ConstraintViolationException;
}
