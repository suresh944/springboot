package com.example.springboot.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "ControlCenter.users")
public class UsersEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column(name = "user_code")
    private Integer userCode;

    @Column(name = "user_loginname")
    private String userLoginName;

    @Column(name = "user_category_id")
    private Integer userCategoryId;


    @Column(name = "employee_id")
    private Integer employeeId;

//    @ManyToOne
//    @JoinColumn(name = "user_category_id")
//    @JsonBackReference
//    private UserCategoryEntity userCategoryEntity;

    //    @Column(name = "user_loginname")

//    private UserCategoryEntity userCategoryEntity;
//    @Column(name = "user_category_id")

/*
//    @Column(name = "user_code")
//    @Column(name = "user_loginname")
    private String userLoginname;

//    @Column(name = "user_password")
    private String userPassword;

    @Column(name = "user_mpin_password")
    private String userMpinPassword;

//    @Column(name = "user_display_name")
    private String userDisplayName;

//    @Column(name = "is_active")
    private Integer isActive;

//    @Column(name = "user_category_id")
    @ManyToOne()
    @JoinColumn(name="user_category_id")
    private UserCategoryEntity userCategoryEntity;

    private Integer userCategoryId;

//    @Column(name = "created_datetime;")
    private Date createdDatetime;;

//    @Column(name = "created_by")
    private Integer createdBy;

//    @Column(name = "modified_datetime")
    private Date modifiedDatetime;

//    @Column(name = "modified_by")
    private Integer modifiedBy;

    */

}
