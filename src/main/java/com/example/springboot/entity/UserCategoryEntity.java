package com.example.springboot.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;

import javax.persistence.*;
import java.util.List;


@Data
@Entity
@Table(name = "ControlCenter.user_category")
public class UserCategoryEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column(name = "user_category_id")
    private Integer userCategoryId;

    @Column(name = "user_category_name")
    private String userCategoryName;

//    @OneToMany( mappedBy = "userCategoryEntity", fetch = FetchType.LAZY )
//    @JsonManagedReference
//    List<UserEntity> userEntityList;

//    @Column(name = "user_category_name")
/*
//    @Column(name = "user_category_id")
//    @Column(name = "user_category_name")
//    private String userCategoryName;

//    @Column(name = "user_category_key")
    private String userCategoryKey;

//    @Column(name = "user_category_description")
    private String userCategoryDescription;

//    @Column(name = "is_active")
    private String isActive;

//    @Column(name = "is_registeration_default")
    private String isRegisterationDefault;

//    @Column(name = "is_admin_default")
    private String isAdminDefault;

//    @Column(name = "created_datetime")
    private String createdDatetime;

//    @Column(name = "created_by")
    private String createdBy;

//    @Column(name = "modified_datetime")
    private String modifiedDatetime;

//    @Column(name = "modified_by")
    private String modifiedBy;

//    @OneToMany(mappedBy="userEntity" , fetch = FetchType.LAZY,cascade = CascadeType.ALL)
////    @OneToMany(  cascade = CascadeType.ALL)
//    @JoinColumn(name = "user_category_id" , referencedColumnName = "user_category_id")*/

//    List<UserEntity> userEntityList = new ArrayList<UserEntity>();

}
