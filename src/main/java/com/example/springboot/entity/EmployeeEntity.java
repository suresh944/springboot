package com.example.springboot.entity;

import lombok.Data;
import javax.persistence.*;

@Data
@Entity(name = "HumanResourceS.Employee")
public class EmployeeEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "employee_id")
    private Integer id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "middle_name")
    private String middleName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "employee_short_name")
    private String employeeShort_name;

    @Column(name = "department_mapping_id")
    private Integer departmentMapping_id;
/*
    @Column(name = "birth_date")
    private String birthDate;
//    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM-dd-yyyy");
//    String date = simpleDateFormat.format(new Date());

    @Column(name = "birth_city_id")
    private String birthCityId;

    @Column(name = "hire_date")
    private String hireDate;

    @Column(name = "qualification_id")
    private String qualificationId;

    @Column(name = "floor_extension_id")
    private String floor_extensionId;

    @Column(name = "employment_status_id")
    private String employmentStatusId;

    @Column(name = "cast_id")
    private String castId;

    @Column(name = "marrital_status_id")
    private String maritalStatusId;

    @Column(name = "anniversary_date")
    private String anniversaryDate;

    @Column(name = "marriage_city_id")
    private String marriageCityId;

    @Column(name = "primary_email_id")
    private String primaryEmailId;

    @Column(name = "primary_phone_number")
    private String primaryPhoneNumber;

    @Column(name = "travel_preferances_id")
    private String travelPreferancesId;

    @Column(name = "vehicle_number")
    private String vehicleNumber;

    @Column(name = "blood_group_id")
    private String bloodGroupId;

    @Column(name = "gender")
    private String gender;

    @Column(name = "created_datetime")
    private String createdDatetime;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "modified_datetime")
    private String modifiedDatetime;

    @Column(name = "modified_by")
    private String modifiedBy;


 */
}
