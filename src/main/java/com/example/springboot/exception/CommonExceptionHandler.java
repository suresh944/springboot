package com.example.springboot.exception;

import com.example.springboot.dto.ResponseDTO;
import com.example.springboot.utils.ResponseGeneratorUtil;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class CommonExceptionHandler extends ResponseEntityExceptionHandler {

    @Autowired
    ResponseGeneratorUtil responseGeneratorUtil = new ResponseGeneratorUtil();

    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<ResponseDTO> handleAllException(UserNotFoundException ex) {
        ResponseEntity<ResponseDTO> user_not_found = new ResponseEntity<ResponseDTO>(responseGeneratorUtil.
        generateResponse(true, HttpStatus.NOT_FOUND.toString(), ex.getMessage(),null) , HttpStatus.NOT_FOUND);
        return user_not_found;
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<ResponseDTO> duplicateUserFound(UserNotFoundException ex) {
        ResponseEntity<ResponseDTO> user_not_found = new ResponseEntity<ResponseDTO>(responseGeneratorUtil.
        generateResponse(true, HttpStatus.NOT_FOUND.toString(), ex.getMessage(),null) , HttpStatus.NOT_FOUND);
        return user_not_found;
    }


}
